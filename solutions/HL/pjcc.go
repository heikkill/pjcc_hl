package main

import (
	"fmt"
	"bufio"
	"io"
	"os"
	"strings"
	"strconv"
	"errors"
	"sort"
	"bytes"
)

type Dude struct {
	name string
	suitableRooms []int
}

type Room struct {
	name string
	num int
	dudes []Dude
	rearranging bool
}

type BySuitableRooms []Dude
func (b BySuitableRooms) Len() int { return len(b) }
func (b BySuitableRooms) Swap(i, j int) { b[i], b[j] = b[j], b[i] }
func (b BySuitableRooms) Less(i, j int) bool { return len(b[i].suitableRooms) < len(b[j].suitableRooms) }

type ByName []Dude
func (b ByName) Len() int { return len(b) }
func (b ByName) Swap(i, j int) { b[i], b[j] = b[j], b[i] }
func (b ByName) Less(i, j int) bool { return b[i].name < b[j].name }

func (r Room) String() string {
	sort.Sort(ByName(r.dudes))
	var buffer bytes.Buffer
	buffer.WriteString(r.name + ": ")
	for i, dude := range r.dudes {
		buffer.WriteString(dude.name)
		if i < len(r.dudes) - 1 {
			buffer.WriteString(", ")
		}
	}
	buffer.WriteString("\n")
	return buffer.String()
}

func contains(a []int, e int) bool {
    for _, v := range a {
        if v == e {
            return true
        }
    }
    return false
}

func (d Dude) AvgInSuitableRooms(rooms []Room) float64 {
	var total int
	for _, num := range d.suitableRooms {
		total += len(rooms[num-1].dudes)
	}
	return float64(total) / float64(len(d.suitableRooms))
}

func (d Dude) getLeastCrowdedRoom(rooms []Room) *Room {
	if len(d.suitableRooms) == 0 {
		return nil
	}
	
	room := &rooms[d.suitableRooms[0]-1]
	for _, num := range d.suitableRooms[1:] {
		if len(rooms[num-1].dudes) < len(room.dudes) {
			room = &rooms[num-1]
		}
	}
	return room
}

func (r *Room) AddToRoom(d Dude, rooms []Room, avgDudeCount float64) {
	r.dudes = append(r.dudes, d)
	
	if float64(len(r.dudes)) > avgDudeCount {
		r.rearranging = true
		r.Rearrange(rooms, avgDudeCount)
		r.rearranging = false
	}
}

func (r *Room) Rearrange(rooms []Room, avgDudeCount float64) {
	sort.Sort(sort.Reverse(BySuitableRooms(r.dudes)))
	for i, _ := range r.dudes {
		moveDude := r.dudes[i]
		if len(moveDude.suitableRooms) == 1 {
			continue
		}
		
		newroom := findNewRoomForDude(moveDude, rooms, r.num)
		if newroom == nil {
			continue
		}
		
		r.dudes = append(r.dudes[:i], r.dudes[i+1:]...)
		newroom.AddToRoom(moveDude, rooms, avgDudeCount)
		return
	}
}

func findNewRoomForDude(dude Dude, rooms []Room, currentRoomNum int) *Room {
	suitableAvg := dude.AvgInSuitableRooms(rooms)
	
	for _, num := range dude.suitableRooms {
		if (rooms[num-1].rearranging || rooms[num-1].num == currentRoomNum) {
			continue
		}
		if float64(len(rooms[num-1].dudes)) < suitableAvg {
			return &rooms[num-1]
		}
	}
	return nil
}

func NewDude(lineStr string, roomsCount int) (Dude, error) {
	if lineStr == "" {
		return Dude{}, errors.New("EOF")
	}
	
	split := strings.Split(lineStr, ",")
	dude := Dude{ split[0], []int{} }
	if len(split) == 1 {
		for i := 1; i <= roomsCount; i++ {
			dude.suitableRooms = append(dude.suitableRooms, i)
		}
		return dude, nil
	}

	nogoRooms := make([]int, len(split)-1, len(split)-1)
	for i, v := range split[1:] {
		nogoRooms[i], _ = strconv.Atoi(v)
	}
	
	for i := 1; i <= roomsCount; i++ {
		if contains(nogoRooms, i) {
			continue
		}
		dude.suitableRooms = append(dude.suitableRooms, i)
	}
	
	return dude, nil
}

func readRooms(scanner *bufio.Scanner) []Room {
	scanner.Scan()
	split := strings.Split(scanner.Text(), ",")
	rooms := make([]Room, len(split), len(split))
	for i, v := range split {
		rooms[i] = Room{ v, i+1, []Dude{}, false }
	}
	return rooms
}

func readDudes(scanner *bufio.Scanner, roomsCount int) []Dude {
	var dudes []Dude
	for scanner.Scan() {
		dude, err := NewDude(scanner.Text(), roomsCount)
		if err == nil {
			dudes = append(dudes, dude)
		} else {
			break
		}
	}
	
	sort.Sort(BySuitableRooms(dudes))
	return dudes
}

func pjcc(r io.Reader, w io.Writer) {
	scanner := bufio.NewScanner(r)
	rooms := readRooms(scanner)
	dudes := readDudes(scanner, len(rooms))
	averageDudeCount := float64(len(dudes)) / float64(len(rooms))
	
	for i, _ := range dudes {
		room := dudes[i].getLeastCrowdedRoom(rooms)
		if room != nil {
			room.AddToRoom(dudes[i], rooms, averageDudeCount)
		}
	}
	
	for _, r := range rooms {
		fmt.Fprint(w, r)
	}
}

func main() {
	pjcc(os.Stdin, os.Stdout)
}